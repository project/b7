***********************************************************
*                         B7 Theme                        *
*                     By: Brad Landis                     *
***********************************************************

----- README.txt -----

This theme is a little different from other themes, because
there are three different themes in the B7 Package.

One major difference is the fact that the css is in two
different files, base.css, and style.css. There is a
style.css for each different theme, but only one base.css

style.css: This file contains the color scheme for the
theme. If you would like to change the colors, this file
is the one that needs to be edited

base.css: This file contains all the details that are
not specific to the color theme. A few exceptions for
colors only occur when the color is not specific to the
color scheme (e.g. Black, White, Shades of Gray).

