<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
  <title> <?php print $head_title ?> </title>
  <meta http-equiv="Content-Style-Type" content="text/css" />
   <?php print $head ?>
   <?php print $styles ?>
   <?php print $scripts ?>
</head>
<body <?php print theme("onload_attribute"); ?> >
<div id="all">
<div id="header" <?php if(!$search_box && !$site_name && !$site_slogan) echo "class=\"center_logo\"" ?>>
  <?php if ($search_box): ?>
    <div id="search-wrap">
      <?php print $search_box; ?>
    </div>
  <?php endif; ?> 
  <?php if ($logo) : ?>
    <a href=" <?php print url() ?> " title="Index Page"><img src=" <?php print($logo) ?> " alt="Logo" /></a>
   <?php endif; ?>
   <?php if ($site_name) : ?>
    <h1 id="site-name"><a href=" <?php print url() ?> " title="Index Page"> <?php print($site_name) ?> </a></h1>
   <?php endif;?>
   <?php if ($site_slogan) : ?>
    <span id="site-slogan"> <?php print($site_slogan) ?> </span>
   <?php endif;?>
  <br class="clear" />
</div>

  <div id="top-nav">
   <?php echo theme('links', $primary_links, array('id' => 'primary')) ?>
   <?php echo theme('links', $secondary_links, array('id' => 'secondary')) ?>
  </div>
  <?php if($header) : ?>
    <div class="sidebar" id="sidebar-top">
      <?php print $header; ?>
    </div>
  <?php endif; ?>

<table id="content"><tbody style="border: none !important;">
  <tr>
     <?php if ($sidebar_left != ""): ?>
      <td class="sidebar" id="sidebar-left">
         <?php print $sidebar_left ?>
      </td>
     <?php endif; ?>
        <td class="main-content" id="content-<?php print $layout ?>">
         <?php if ($title != ""): ?>
          <h2 class="content-title"> <?php print $title ?> </h2>
         <?php endif; ?>
         <?php if ($tabs != ""): ?>
           <?php print $tabs ?>
         <?php endif; ?>
         <?php if ($mission != ""): ?>
          <p id="mission"> <?php print $mission ?> </p>
         <?php endif; ?>
         <?php if ($help != ""): ?>
          <p id="help"> <?php print $help ?> </p>
         <?php endif; ?>
         <?php if ($messages != ""): ?>
          <div id="message"> <?php print $messages ?> </div>
         <?php endif; ?>
        <!-- start main content -->
         <?php print($content) ?>
        <!-- end main content -->
        </td><!-- mainContent -->
     <?php if ($sidebar_right != ""): ?>
    <td class="sidebar" id="sidebar-right">
         <?php print $sidebar_right ?>
    </td>
     <?php endif; ?>
  </tr>
</tbody></table>
<?php if ($breadcrumb != ""): ?>
   <?php print $breadcrumb ?>
<?php endif; ?>
  <?php print $closure;?>
  </div>
  <div id="footer">
    <?php if ($footer_message) : ?>
      <p> <?php print $footer_message;?> </p>
    <?php endif; ?>
  </div><!-- footer -->
  </body>
</html>

